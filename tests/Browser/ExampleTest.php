<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Chrome;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    use DatabaseMigrations;
      public function setUp(): void 
            {
                parent::setUp();
                $this->artisan('db:seed');
            }

/**
     * A basic browser test example.
     *
     * @return void
     */
    public function testPasswordIncorrect()
    {
        // $user = factory(User::class)->create([
        //     'email' => 'admin@admin.com',
        //     'id_role'=>1]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Remember Me')
                    ->type('email', 'admin@admin.com')
                    ->type('password', '133323')
                    ->press('Login')
                    ->assertPathIs('/login')
                    ->screenshot('exempletestScreenshot')
                    ->assertSee('These credentials do not match our records')
                    ->screenshot('exempleScreenshot.png');

                    
        });
    }

     public function testMessageErrorIncorrectOfCreditials()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Remember Me')
                    ->type('email', 'admin@admin.com')
                    ->type('password', '133323')
                    ->press('Login')
                    ->assertPathIs('/login')
                    ->assertDontSee('These credentials do not match our records error');
                    // ->assertAuthenticated();
        });
    }
    /**
     * A basic browser test example.
     *
     * @return void
     */
    
 public function testLoginGestionnaire()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Remember Me')
                    ->type('email', 'gestionnaire@gestionnaire.com')
                    ->type('password', 'gestionnaire@gestionnaire.com')
                    ->press('Login')
                    ->assertPathIs('/home')
                    ->screenshot('LoginManagerScreenshot.png')
                    // ->assertSee('These credentials do not match our records');
                    ->assertAuthenticated()
                    ->logout()
                    ->assertPathIs('/_dusk/logout')
                    ->assertGuest()
                    ->screenshot('LogoutManagerScreenshot.png');
        });
    }
 public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Remember Me')
                    ->type('email', 'admin@admin.com')
                    ->type('password', 'admin@admin.com')
                    ->press('Login')
                    ->assertPathIs('/home')
                    ->screenshot('LoginScreenshot.png')
                    ->assertAuthenticated();

        });
    }

            public function testRegister()
                {
                    // $role = factory(Role::class)->create();
                    // $user = [
                    //             'text' => 'New task text',
                    //             'user_id' => $user->id
                    //         ];
                    $this->browse(function (Browser $browser) {
                        $browser->visit('/register')
                                ->screenshot('RegisterloginScreenshot.png')
                                ->type('firstName', 'meryem')
                                ->type('lastName', 'bellorhzal')
                                ->type('id_role', '2')
                                ->type('email', 'meryembellorhzal@laravel.com')
                                ->type('password', 'ahmedkhan')
                                ->type('password_confirmation', 'ahmedkhan')
                                ->press('Register')
                                ->assertSee('user was created')
                                ->screenshot('RegisterScreenshot.png');
                    });
                }
    // public function testBasicExample()
    // {
    //     // $user = factory(User::class)->create([
    //     //     'email' => 'admin@admin.com',
    //     //     'id_role'=>1]);

    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('/login')
    //                 ->assertSee('Remember Me')
    //                 ->type('email', 'admin@admin.com')
    //                 ->type('password', 'admin@admin.com')
    //                 ->press('Login')
    //                 ->assertPathIs('/login')
    //                 ->assertSee('Admin!')
    //                 ->assertAuthenticated();
    //     });
    // }
}
