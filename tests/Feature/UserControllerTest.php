<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
// use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
   use DatabaseMigrations;
      public function setUp(): void 
            {
                parent::setUp();
                $this->artisan('db:seed');
            }


    public function testloggedTest(){
        $response = $this->get('/admin/users')
        ->assertRedirect('/login');
    }
    public function testUsersHasAdminEmail(){
            $this->assertDatabaseHas('users', [
            'email' => 'admin@admin.com'
    ]);
    }
}
    
