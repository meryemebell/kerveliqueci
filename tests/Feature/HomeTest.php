<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHomeLogin()
    {
        $response = $this->get('/home');

        // $response->assertSeeText('Dashboard');
        $response->assertRedirect('/login');
    }

    public function testWelcom(){
        $response=$this->get('/');
        $response->assertSee('Kervelique Ferme');
        $response->assertStatus(200);
    }
}
