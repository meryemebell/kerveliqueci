<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        User::truncate();
        User::create([
            'firstName'=>'admin',
            'lastName'=>'adminKervelique',
            'email'=>'admin@admin.com',
            'password'=>Hash::make('admin@admin.com'),
            'id_role'=>'1'
        ]);
        User::create(
        [
            'firstName'=>'gestionnaire',
            'lastName'=>'gestionnaireKervelique',
            'email'=>'gestionnaire@gestionnaire.com',
            'password'=>Hash::make('gestionnaire@gestionnaire.com'),
            'id_role'=>'3'
        ]);
    }
}
