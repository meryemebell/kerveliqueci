<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create dafaults roles
        Role::truncate();

        Role::create(['role_name'=>'admin']);
        Role::create(['role_name'=>'owner']);
        Role::create(['role_name'=>'manager']);
    }
}
