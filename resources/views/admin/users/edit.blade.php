@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modifier user {{$user->firstName}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{route('admin.users.update',$user)}}" method="POST">
                    @method('PATCH')
                    @foreach($roles as $role)
                        <div class="form-group form-check">
                            <input type="radio" class="form-check-input" name="role" value="{{ $role->role_name }}" id="{{$role->id}}" @if ($user->id_role == $role->id) checked @endif>
                        <label for="{{$role->id}}" class="form-check-label">{{$role->role_name}}</labe>
                        </div>
                    @endforeach
                    <button type="submit" class="btn btn-primary">Modifier le role</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
